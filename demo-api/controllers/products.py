from models.modules.core import logger, request, InvalidUsage
from models.dbmodels.product_model import Product, db, products_scheme, exc

class Products():

    def getall():
        products = Product.query.all()
        return {
            "message": products_scheme.dump(products)
        }

    def add_product():
        name = request.json['name']
        description = request.json['description']
        price = request.json['price']
        qty = request.json['qty']
        try:
            exists = Product.query.filter_by(name = name).first()
            if exists:
                raise Exception('product exists')

            new_product = Product(name, description, price, qty)
            db.session.add(new_product)
            db.session.commit()
            db.session.close()
        except exc.SQLAlchemyError as e:
            raise Exception(e._message)
        except Exception as e:
            raise Exception(e._message)