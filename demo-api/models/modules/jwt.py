from .core import make_response, request, app, jsonify, InvalidUsage
import jwt
import datetime
from functools import wraps

userInfo = {}

def validate_token(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.headers.get('Authorization')
        if not auth:
            return make_response({
                "error": "Authorization header is missing from request"
            }, 400)
        
        token = auth.split(' ')[1]
        if not token:
            return make_response({
                "error": "Token is missing from request"
            }, 400)

        try: 
            data = jwt.decode(token, app.config['SECRET_KEY'])
        except: 
            return make_response({
                "error": "Token is invalid"
            }, 500)
        
        return f(*args, **kwargs)
    return decorated

def generatejwt(data): 
        token = jwt.encode({
            'user': data,
            'exp': datetime.datetime.now() + datetime.timedelta(minutes=30)
        }, app.config['SECRET_KEY'])
        return token.decode('UTF-8')
        