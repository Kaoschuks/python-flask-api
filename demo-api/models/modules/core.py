from flask import Flask, request, jsonify, make_response, url_for
from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer, SignatureExpired
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_compress import Compress
# from flask_cache import Cache
import os
import logging as logger
logger.basicConfig(level="DEBUG")

app = Flask(__name__)
app.config.from_pyfile('config.cfg')
# cache = Cache()
Compress(app)
# cache.init_app(app)

class InvalidUsage(Exception):
    status_code = 500

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['error'] = self.message
        return rv