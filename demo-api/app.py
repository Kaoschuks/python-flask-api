from models.modules.core import app
from views.route import *


if __name__ == "__main__":
    app.run(
        host = "0.0.0.0",
        port = "5000",
        debug = True,
        use_reloader = True
    )