from views.users_route import *
from views.products_route import *


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/', methods=['GET'])
def get():
    return jsonify({
        'message': 'Hoolo world'
    })