from models.modules.core import app, jsonify, InvalidUsage
from controllers.products import Products
from models.modules.jwt import *

@app.route('/products', methods=['GET'])
@validate_token
def getallProducts():
    try:
        return Products.getall()
    except Exception as e:
        raise InvalidUsage(str(e), status_code=500)

@app.route('/products', methods=['POST'])
@validate_token
def addProducts():
    try:
        return Products.add_product()
    except Exception as e:
        raise InvalidUsage(str(e), status_code=500)